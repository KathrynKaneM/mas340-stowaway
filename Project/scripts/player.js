//Player info

var ophBody = ""; //stores name of file for body image
var ophHead; //stores name of file for head image- defaults to 0 for testing


var bodies = ["cloak_up"]

var firstStealth = true; //has the player done the stealth game yet

//ally variables --> turn into a object/dict
// name = [met?, ally, romance]

var gioele = {
	met: false,
	ally: 0,
	love: 0
	};
var stephanos = {
	met : false,
	ally : 0,
	love : 0
	};
var antonio = {
	met : false,
	ally : 0
	};

//initialise localstorage items if not yet initialised - testing purposes
if(localStorage.getItem("suspicion") === null){
	localStorage.suspicion = 10;
}

if(localStorage.getItem("cloak") === null){
	localStorage.cloak = JSON.stringify(false);
}
if(localStorage.getItem("location") === null){
	localStorage.location = "bedroom";
}
if(localStorage.getItem("locationTarget") === null){
	localStorage.locationTarget = "galley";
}
if(localStorage.getItem("OpheliaPort") === null){
	localStorage.OpheliaPort = JSON.stringify(1);
}
if(localStorage.getItem("locations") === null){
	localStorage.locations = JSON.stringify([]);
}
if(localStorage.getItem("firstStealth") === null){
	localStorage.firstStealth = JSON.stringify(true);
}
if(localStorage.getItem("gioele") === null){
	localStorage.gioele = JSON.stringify(gioele);
}
if(localStorage.getItem("stephanos") === null){
	localStorage.stephanos = JSON.stringify(stephanos);
}
if(localStorage.getItem("antonio") === null){
	localStorage.antonio = JSON.stringify(antonio);
}
if(localStorage.getItem("bodyImg") === null){
	localStorage.bodyImg = "cloakDress";
}
if(localStorage.getItem("headImg") === null){
	localStorage.headImg = "hair0";
}
if(localStorage.getItem("mute") === null){
	localStorage.mute = JSON.stringify(false);
}



function opheliaImage(facing){
	ophBody = localStorage.getItem("bodyImg");
	ophHead = localStorage.getItem("headImg");
	//console.log(ophHead);
	
	//add div to contain
	$('#stage').append('<div id="char' + facing + '"></div>');
	$('#char'+ facing).html('<img class="ophelia" src=' + '"images/' + ophHead + '.png">');
	$('#char'+ facing).append('<img class="ophelia" src=' + '"images/' + ophBody + '.png">');
	
}

function doStealth(loc, locations){ //call on origin page to determine if stealth game is needed
	var stealth = true;
	for(var x = 0; x < locations.length; x++ ) {
		if (locations[x] == loc) { //player has been to the chosen location before
			switch(loc){
				case "doctors":
					if(stephanos.met) { //if player has met then no stealth required
						stealth = false;
						localStorage.locationTarget = "doctors";
					} else {
						stealth = true;
					}
					break;
				case "galley":
					if(antonio.met) { //if player has met then no stealth required
						stealth = false;
						localStorage.locationTarget = "galley";
					} else {
						stealth = true;
					}
					break;
				case "deck":
					if(gioele.met) { //if player has met then no stealth required
						stealth = false;
						localStorage.locationTarget = "deck";
					} else {
						stealth = true;
					}
					break;
			}
		} else { //player not been to chosen location before
			stealth = true;
		}
	}
	return stealth;
}