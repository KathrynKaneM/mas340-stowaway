//essential functions & core variables across pages which don't specify for ophelia only

//these are changed with each dialogue
//var per = 0; 
//var exp = 0;


if(localStorage.getItem("musicTime") === null){
	localStorage.musicTime = 0;
}
if(localStorage.getItem("day") === null){
	localStorage.day = JSON.stringify(1);
}

function nextPage(pageName) {
	$('#stage').fadeOut(1000, function() {
		var musicTime = document.getElementById("bgMusic").currentTime;
		localStorage.setItem('musicTime', musicTime);
		window.location = pageName;
	})

}

function startTimer(dur, endFunc) {
		$('#bar-inner').animate({
			width: 0
		},{
			easing: 'linear',
			duration: dur,
			complete: endFunc //function to be run when finished
		})
	}

	
function getPortrait(id, per, exp) {
	//0 = neutral
	//1 = happy
	//2 = angry
	//3 (ophelia only) = shock
	
	var avatars = [
		["blank"], //0 default (no avatar)
		["intro1", "intro2", "intro3", "intro4"], //1 ophelia1 (intro)
		["hair1.1", "hair1.2", "hair1.3", "hair1.4"], //2 ophelia2
		["hair2.1", "hair2.2", "hair2.3", "hair2.4"], //3 ophelia3
		["hair3.1", "hair3.2", "hair3.3", "hair3.4"], //4 ophelia4
		["maid"], //5 maid
		["ant1", "ant2", "ant3"], //6 antonio
		["gio1", "gio2", "gio3"], //7 gioele
		["ste1", "ste2", "ste3"], //8 stephanos
		["father1", "father2", "father3", "father4" ]
	]
	
	if ( per == 0) {
		document.getElementById(id).src="images/avatars/"+ avatars[per][exp] + ".png";
		
	} else {
		document.getElementById(id).src="images/avatars/"+ avatars[per][exp] + ".jpg";
	}
	
}

function choicesRender(choices) {
		var id;
		for (id = 0; id < choices.length; id++){
			$('#textBox').append('<div id="choice'+ id+ '" class="choices">'+ choices[id] + '</div>');
		}
	
}
	
